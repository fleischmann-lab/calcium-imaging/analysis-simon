---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.4.2
  kernelspec:
    display_name: simon
    language: python
    name: simon
---

# Import all packages needed 

```python
import os
import math
import xml.etree.ElementTree as ET
import datetime
from dateutil.parser import parser

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from ipyfilechooser import FileChooser
from ipywidgets import interact
from calimag.Parse import MicroscopeParsing
```

# Parameters 

```python
odorids = [
    5,
    2,
    8,
    4,
    9,
    3,
    7,
    6,
    1,
    9,
    2,
    6,
    4,
    1,
    8,
    7,
    3,
    5,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    5,
    2,
    8,
    4,
    9,
    3,
    7,
    6,
    1,
    9,
    2,
    6,
    4,
    1,
    8,
    7,
    3,
    5,
]
triallength = 30  # sec
numtrials = len(odorids)
print(f"Number of odors: {len(set(odorids))}")
print(f"Number of trials: {numtrials}")
```

# Select files

```python
# Create new FileChooser:
# Path: current directory
# File: test.txt
# Title: <b>FileChooser example</b>
# Show hidden files: no
# Use the default path and filename as selection: yes
f_metadata = FileChooser(
    os.getcwd(),
    filename="TSeries-07082019-1115_-001.xml",
    title="<b>Select scope metadata file</b>",
    show_hidden=False,
    select_default=True,
)
display(f_metadata)

# Create new FileChooser:
# Path: current directory
# File: test.txt
# Title: <b>FileChooser example</b>
# Show hidden files: no
# Use the default path and filename as selection: yes
f_neural = FileChooser(
    os.getcwd(),
    filename="Fcbnsi.npy",
    title="<b>Select neural data file</b>",
    show_hidden=False,
    select_default=True,
)
display(f_neural)
```

# Import Metadata from the scope (frames + duration of experiment)

```python
parser = MicroscopeParsing()
time_frames = parser.get_frames_timestamps_from_xml(f_metadata.selected, 1)

# Find number of frames
numframes = len(time_frames)
framespertrial = int(numframes / numtrials)
print(f"Total number of frames: {numframes}")
print(f"Number of frames/trial: {framespertrial}")

# Calculate frame rate based on total experiment duration from serial data
expduration = triallength * numtrials
frame_period = pd.to_timedelta(expduration / numframes, unit="s")
print(f"One frame recorded every {frame_period.total_seconds()*1000:.3f} milliseconds")
print(f"Frame rate: {1/frame_period.total_seconds():.3f} Hz")
```

# Import neural data

```python
Fcbnsi = np.transpose(np.load(f_neural.selected))
number_cells = Fcbnsi.shape[1]
print(f"Total number of cells: {number_cells}")

DfOverF = pd.DataFrame(data=Fcbnsi, index=time_frames)
DfOverF
```

# Add odor id and trial number to $\frac{\Delta F}{F}$ values 

```python
# Add column with odor ID
DfOverF["Odor_ID"] = np.repeat(odorids, framespertrial)

# Add column with trial number
trials = np.repeat(np.arange(1, numtrials + 1), framespertrial)
DfOverF["Trial_Number"] = trials
DfOverF
```

# Choose period to analyse

```python
# Choose periods to analyze relative to odor presentation.
# Strategy is to add column to matrix
# which marks periods of interest for easy indexing.

# Based of experimental parameters, input odor on time relative to trial start.
# ToDo: extract automatically from serial data.
odor_start_time = 10  # sec

# Input duration following odor on time that will be used for analysis.
odor_period = 3  # sec

odor_start_frame = math.floor(odor_start_time / frame_period.total_seconds())
odor_period_frames = math.floor(odor_period / frame_period.total_seconds())

startzeros = np.repeat(False, odor_start_frame)
ones = np.repeat(True, odor_period_frames)
endzeros = np.repeat(False, framespertrial - odor_start_frame - odor_period_frames)
onetrial = np.concatenate([startzeros, ones, endzeros])
alltrials = np.tile(onetrial, numtrials)

# Make new column including only data points at odor period
DfOverF["is_odor_present"] = alltrials
DfOverF
```

# Plot cells traces

```python
%matplotlib widget
pd.plotting.register_matplotlib_converters()


@interact(cell=(0, number_cells - 1, 1))
def plot_cell_trace(cell):

    cell_trace_fig = "Cells traces"
    plt.close(cell_trace_fig)
    fig = plt.figure(cell_trace_fig, figsize=(10, 10))

    ColorMapName = "tab10"
    # To get a color for a point between [0, 1]
    cmap = mpl.cm.get_cmap(ColorMapName)

    signal = DfOverF[cell]
    threshold = signal.mean() + signal.std()

    extracted = signal[signal > threshold]
    plt.plot(signal, label="signal", color=cmap(0), figure=fig)
    plt.axhline(
        threshold,
        color=cmap(1),
        linestyle="dashed",
        label="threshold",
        linewidth=1,
        figure=fig,
    )
    plt.scatter(
        extracted.index,
        extracted.values,
        label="extracted",
        color=cmap(2),
        marker="o",
        facecolors="none",
        figure=fig,
    )

    # Plot trials starts
    for name, group in DfOverF.groupby("Trial_Number"):
        if name == 1:
            plt.axvline(
                x=group.index[0],
                linestyle="dashed",
                linewidth=1,
                color="Gainsboro",
                label="Trial start",
                figure=fig,
            )
        else:
            plt.axvline(
                x=group.index[0],
                linestyle="dashed",
                linewidth=1,
                color="Gainsboro",
                figure=fig,
            )

    ax = plt.gca()
    ax.xaxis.set_major_formatter(mpl.dates.DateFormatter("%b %d %Y %H:%M:%S"))
    plt.gcf().autofmt_xdate()
    plt.legend(loc="best")
    plt.show()
```

# Active cells

```python
def is_active_or_not(signal, threshold):
    """Decide if cell is active or not"""
    extracted_signal = signal[signal > threshold]
    if len(extracted_signal) >= 0.95 * len(signal):
        return True
    else:
        return False


is_active_or_not_cells = pd.DataFrame()  # index=np.arange(1, numtrials + 1))
weight_activity_cells = pd.DataFrame()  # index=np.arange(1, numtrials + 1))
Odor_ID = []
Trial_Number = []
for columnName, columnData in DfOverF.iteritems():
    if str(columnName).isnumeric():  # Only cell columns
        threshold = columnData.mean() + columnData.std()  # Threshold on all frames

        is_active_or_not_vect = []
        weight_activity_vect = []
        for name, DfOverF_Trial in DfOverF.groupby("Trial_Number"):
            signal = DfOverF_Trial[DfOverF_Trial["is_odor_present"]].get(columnName)
            is_active_or_not_vect.append(is_active_or_not(signal.values, threshold))
            weight_activity_vect.append(signal.mean() / columnData.max())

            if columnName == 0:  # Do it only once
                Odor_ID.append(DfOverF_Trial["Odor_ID"].iloc[0])
                Trial_Number.append(DfOverF_Trial["Trial_Number"].iloc[0])

        is_active_or_not_cells[columnName] = is_active_or_not_vect
        weight_activity_cells[columnName] = weight_activity_vect

is_active_or_not_cells["Odor_ID"] = Odor_ID
is_active_or_not_cells["Trial_Number"] = Trial_Number
weight_activity_cells["Odor_ID"] = Odor_ID
weight_activity_cells["Trial_Number"] = Trial_Number
weight_activity_cells
```

```python
%matplotlib widget


def plot_active_cells(
    data_matrix,
    convert_to_type=np.float_,
    title="",
    xlabel="",
    ylabel="",
    cbar_ticks=None,
    xgrid=None,
):
    """Heatmap function to plot active cells"""
    columns_to_drop = ["Odor_ID", "Trial_Number"]
    fig = plt.figure(figsize=(10, 10))
    im = plt.imshow(
        data_matrix.drop(columns_to_drop, axis=1).transpose().astype(convert_to_type),
        cmap="viridis",
    )
    ax = plt.gca()
    plt.title(title, fontsize=20)
    plt.xlabel(xlabel, fontsize=20)
    plt.ylabel(ylabel, fontsize=20)
    if cbar_ticks:
        cbar = plt.colorbar(im, ticks=cbar_ticks.get("ticks"))
        cbar.ax.set_yticklabels(cbar_ticks.get("set_yticklabels"))
    else:
        cbar = plt.colorbar(im)
    if xgrid:
        major_x = np.arange(
            min(odorids) - 1.5 + (len(odorids) / max(odorids)) / 2,
            len(odorids),
            len(odorids) / max(odorids),
        )
        print(f"major_x: {major_x}")
        major_x_label = np.arange(min(odorids), max(odorids), 1)
        print(f"major_x_label: {major_x_label}")
        minor_x = np.arange(
            min(odorids) - 1.5, len(odorids), len(odorids) / max(odorids)
        )
        print(f"minor_x: {minor_x}")
        ax.set_xticks(
            major_x, minor=False,
        )  # Major ticks
        ax.set_xticklabels(major_x_label)  # Labels for major ticks
        ax.set_xticks(
            minor_x, minor=True,
        )  # Minor ticks
        ax.xaxis.grid(
            True, which="minor", color="w", linestyle="-", linewidth=1
        )  # Gridlines based on minor ticks


# Plot active/not active matrix
plot_active_cells(
    is_active_or_not_cells,
    convert_to_type=np.int_,
    title="Active/not active cells",
    xlabel="Trials",
    ylabel="Cells",
    cbar_ticks={"ticks": [0, 1], "set_yticklabels": ["Not active cell", "Active cell"]},
)

# Plot weight activity matrix
plot_active_cells(
    weight_activity_cells, title="Cells activity", xlabel="Trials", ylabel="Cells",
)
```

```python
# Organize by odor ID
plot_active_cells(
    is_active_or_not_cells.sort_values(by="Odor_ID", axis="index"),
    convert_to_type=np.int_,
    title="Active/not active cells",
    xlabel="Odor ID",
    ylabel="Cells",
    cbar_ticks={"ticks": [0, 1], "set_yticklabels": ["Not active cell", "Active cell"]},
    xgrid=True,
)
plot_active_cells(
    weight_activity_cells.sort_values(by="Odor_ID", axis="index"),
    title="Cells activity",
    xlabel="Odor ID",
    ylabel="Cells",
    xgrid=True,
)
```

from numba.core.decorators import njit
import numpy as np
from scipy.spatial.distance import squareform
import numba
from numba import jit

from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler, StandardScaler
scaler = MinMaxScaler()
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix

def MeanResp(array, numTrialsToKeep = 8, win = [45,60]): 

	
	#array: trials, cells, odors, frames

	ntrials, ncells, nodors, nframes = array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps = array[:numTrialsToKeep][...,win[0]:win[1]].mean(-1)

	return resps

def ConfusionMatrix(X, y, n_splits, n_repeats,C, class_weight, gamma, kernel):
    y_predd =[]
    y_testt =[]
    svm_class = SVC(C=C, class_weight=class_weight, gamma=gamma, kernel=kernel)
    rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
    for train_index, test_index in rskf.split(X, y):
        X_train = X[train_index]
        X_test = X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)
        svm_class.fit(X_train, y_train)
        y_pred = svm_class.predict(X_test)
        # print(y_pred)
        # print(y_test)
        y_predd.append(y_pred)
        y_testt.append(y_test)
    y_testt=np.concatenate(y_testt,0)
    y_predd=np.concatenate(y_predd,0)
    confusion = confusion_matrix(y_testt, y_predd)
    normalize1 = confusion / (n_splits*n_repeats)

    return normalize1, confusion


def SVModor(X, y, n_splits, n_repeats):
    scores = []
    meanscore=[]
    svm_class = SVC(kernel='linear')
    rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
    for train_index, test_index in rskf.split(X, y):
        X_train = X[train_index]
        X_test = X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)
        svm_class.fit(X_train, y_train)
        y_pred = svm_class.predict(X_test)
        scores.append(accuracy_score(y_test, y_pred)[None])
    meanscore= np.mean(scores)

    return scores, meanscore

# @jit
def SVMslidewindow (X, Xz, y, Numslidewin, n_splits, n_repeats):
    swinscore=[]
    meanscore = []
    swinnomeanscore = []

    for i in range(Numslidewin):
        scores = []
        svm_class = SVC(kernel='linear')
        rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
        for train_index, test_index in rskf.split(X, y):
            X_train = X[train_index]
            X_test = Xz[:,:,i][test_index]
            y_train, y_test = y[train_index], y[test_index]
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
            svm_class.fit(X_train, y_train)
            y_pred = svm_class.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred)[None])
        meanscore= np.mean(scores)
        swinscore.append(meanscore[None])
        swinnomeanscore.append(scores)
    swinscore=np.concatenate(swinscore,0)
    swinnomeanscore=np.concatenate(swinnomeanscore,1)

    return swinscore, swinnomeanscore

# @jit(nopython=False)
def SVMrandselectcells (X, y, numcells, n_splits, n_repeats):
    swinscore=[]
    meanscore = []

    for i in range(1,numcells):
        index = np.random.choice(X.shape[1], i, replace=False)  
        X_rand = X[:,index]
        scores = []
        svm_class = SVC(kernel='linear')
        rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
        for train_index, test_index in rskf.split(X_rand, y):
            X_train = X_rand[train_index]
            X_test = X_rand[test_index]
            y_train, y_test = y[train_index], y[test_index]
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
            svm_class.fit(X_train, y_train)
            y_pred = svm_class.predict(X_test)
            scores.append(accuracy_score(y_test, y_pred)[None])
        meanscore= np.mean(scores)
        swinscore.append(meanscore[None])
    swinscore=np.concatenate(swinscore,0)
    return swinscore
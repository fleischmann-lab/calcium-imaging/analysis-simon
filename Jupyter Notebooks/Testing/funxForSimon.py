def getRespsRegular(array, numTrialsToKeep=6, blinewin=[87, 110], odorwin=[115, 130]):
	# array: trials, cells, odors, frames

	# average F every this frames after odor onset
	# numTrialsToKeep: optional

	ntrials, ncells, nodors, nframes=array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps=array[:numTrialsToKeep][..., odorwin[0]:odorwin[1]].mean(-1) - array[:numTrialsToKeep][...,
	                                                                     blinewin[0]:blinewin[1]].mean(-1)

	return resps


def thresholdOutlierResponses(array, lthresh=1, hthresh=99):
	threshh=np.percentile(array, hthresh)
	threshl=np.percentile(array, lthresh)

	excludeE=np.unique(np.argwhere((array > threshh).sum(2)).squeeze()[:, 1])
	excludeI=np.unique(np.argwhere((array < threshl).sum(2)).squeeze()[:, 1])

	exclude=np.union1d(excludeE, excludeI)

	array=np.delete(array, exclude, axis=1)
	return array


def getSNR(resps, numTrialsToKeep=7):
	# resps is array shaped trials, cells, odors

	odormeans=resps.mean(0)
	resids=(odormeans[None] - resps)
	ratio=np.mean(((odormeans ** 2) ** .5 / ((resids ** 2).mean(0) ** .5)), 1)
	cellSNR=10 * np.log10(ratio)
	return cellSNR


def get_sigCors(data):
	"""data: pseudopopulation of shape(trials, cells, odors)"""
	data2=data.mean(0)
	return squareform(np.corrcoef(data2), checks=False)


def get_EnsembleCors(data):
	"""data: pseudopopulation of shape(trials, cells, odors)"""
	data2=data.mean(0).T
	return squareform(np.corrcoef(data2), checks=False)


def getLS(data, No=22):
	# neg resps zeroed
	input_=data.copy().T  # input: odors by neurons by trials

	# zero negatives# or, take the modulo.
	input_=input_.mean(-1)

	input_[input_ < 0]=0
	input_=np.delete(input_, np.argwhere(np.mean(input_ ** 2, 0) == 0), axis=1)
	numerator=1 - input_.mean(0) ** 2 / np.mean(input_ ** 2, 0)
	denominator=1 - 1 / No

	LS=numerator / denominator
	return LS


def getPS_zero(data, No=22):
	input_=data.T.copy()  # input: odors by neurons by trials

	# zero negatives# or, take the modulo.
	input_=input_.mean(-1)
	input_[input_ < 0]=0
	input_=np.delete(input_, np.argwhere(np.mean(input_ ** 2, 0) == 0), axis=1)
	# here, simply flip odor and neuron dimension
	input_=input_.T
	numerator=1 - input_.mean(0) ** 2 / np.mean(input_ ** 2, 0)
	denominator=1 - 1 / No

	PS=numerator / denominator
	return PS


def getPS_abs(data, No=22):
	input_=data.T.copy()  # input: odors by neurons by trials

	# zero negatives# or, take the modulo.
	input_=np.abs(input_.mean(-1))
	input_=np.delete(input_, np.argwhere(np.mean(input_ ** 2, 0) == 0), axis=1)
	# here, simply flip odor and neuron dimension
	input_=input_.T
	numerator=1 - input_.mean(0) ** 2 / np.mean(input_ ** 2, 0)
	denominator=1 - 1 / No

	PS=numerator / denominator
	return PS

def getNoiseCovarianceMatrix_timeSlice(array):
    # trials , cells, odors
    ntrials, ncells, nodors = array.shape
    means = array.mean(0) #     allTimepointsbyCells = array.swapaxes(1,2).reshape(-1, ncells)
    #     stackedResiduals = (means[None][:,:]-array[:,:]).reshape(-1, ncells)
    #     residcovmat = np.cov(stackedResiduals.T)
    residstack = np.dstack([means-array[trial] for trial in range(ntrials)])
    residcovmat =  np.dstack([np.cov(residstack[:,od]) for od in range(22)]).mean(-1)
    return residcovmat 
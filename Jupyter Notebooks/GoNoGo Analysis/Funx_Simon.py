import sys
import os

import numpy as np
import pandas as pd
from glob import glob
import seaborn as sns
from matplotlib import pylab as plt
from scipy.spatial.distance import squareform
from numba import njit
from numpy import save
from numpy import load
sns.set_style('ticks')
sns.set_context('talk')

def GetFluoMeanValueDistribution(array, lthresh, hthresh, title, Fluorophore):
    
    Greencellsmean= array.mean(1)
    Greencellsmean= Greencellsmean[...,lthresh:hthresh]
    Greencellsmean= Greencellsmean.mean(2)
    Greencellsmean= Greencellsmean.mean(1)
    Greencellsmean.shape

    Pm=plt.hist(Greencellsmean, bins = 30, color = Fluorophore)
    plt.title(title)
    plt.xlabel('Fluoresence value')
    plt.ylabel('number of cells')

def rastorplotmeanresponse(filteredDataArray, odornumber, odorID, vmin, vmax):
	Respmean = filteredDataArray.mean(1)
	Respmean_odor1 = Respmean[:,odornumber,:] #second dimension is the odor id 

	cg = sns.clustermap(data=Respmean_odor1, col_cluster=False, metric="correlation", cbar_pos=None, vmin=vmin, vmax=vmax)
	cg.ax_row_dendrogram.set_visible(False)
	plt.axvline(22.6, color = 'w')
	plt.axvline(31.7, color = 'w')
	# plt.xlabel('frames')
	# plt.ylabel('cellID')
	# plt.title(odorID)

def correlogram (filteredDataArray, odorlow, odorup, blinelow, blineup, numtrials, numstim):
	
	odor = filteredDataArray[...,odorlow:odorup].mean(-1)
	bline = filteredDataArray[...,blinelow:blineup].mean(-1)
	respscorr= odor-bline

	correlogramm = np.corrcoef(respscorr.transpose(0,2,1).reshape(respscorr.shape[0],-1).T[:])
	
	plt.figure(figsize=(15,15))
	sns.heatmap(np.corrcoef(respscorr.transpose(0,2,1).reshape(respscorr.shape[0],-1).T[:]), square = True, xticklabels=[], yticklabels=[])
	for i in range(numstim):
		plt.axvline(numtrials*i+numtrials, color = 'w', linewidth = 1)
		plt.axhline(numtrials*i+numtrials, color = 'w', linewidth = 1)
	return correlogramm

def correlogrammean (filteredDataArray, odorlow, odorup, blinelow, blineup, numtrials, numstim, cellnum, times):
	odor = filteredDataArray[...,odorlow:odorup].mean(-1)
	bline = filteredDataArray[...,blinelow:blineup].mean(-1)
	respscorr= odor-bline
	scores=[]
	for i in range(times):
		index = np.random.choice(respscorr.shape[0], cellnum, replace=False)
		respcorrran=respscorr[index,:,:]
		correlogramm = np.corrcoef(respcorrran.transpose(0,2,1).reshape(respscorr.shape[0],-1).T[:])
		scores.append(correlogramm[None])

	return scores

def meanpool2d(x,trialnum):
    h,w = x.shape
    out = np.zeros((h//trialnum,w//trialnum))
    for i in range(h//trialnum):
        for j in range(w//trialnum):
            out[i][j] = np.mean(x[trialnum*i:trialnum*i+trialnum,trialnum*j:trialnum*j+trialnum])
    return out

def getRespsRegular(array, numTrialsToKeep, blinewin, odorwin): 
	#baseline: 5.5-9 seconds, odorwindow: 10-13.3 seconds. numTrialsToKeep = 6 removed
	
	#array: trials, cells, odors, frames

	# average F every this frames after odor onset
	# numTrialsToKeep: optional 

	ntrials, ncells, nodors, nframes = array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps = array[:numTrialsToKeep][...,odorwin[0]:odorwin[1]].mean(-1) -  array[:numTrialsToKeep][..., blinewin[0]:blinewin[1]].mean(-1)
	return resps

def getRespsRegularnobsline(array, numTrialsToKeep, odorwin): 
	#baseline: 5.5-9 seconds, odorwindow: 10-13.3 seconds. numTrialsToKeep = 6 removed
	
	#array: trials, cells, odors, frames

	# average F every this frames after odor onset
	# numTrialsToKeep: optional 

	ntrials, ncells, nodors, nframes = array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps = array[:numTrialsToKeep][...,odorwin[0]:odorwin[1]].mean(-1)
	return resps

def percentresponsivecells(Array, threshold):
	cellNumber = np.count_nonzero(Array, axis=1)
	cellNumber = cellNumber[0,0]
	for i in range (cellNumber):
		Activecells = np.count_nonzero(Array>threshold, axis=1)
		Inhibitcells = np.count_nonzero(Array<-threshold, axis=1)
	ActivecellsP= (Activecells/cellNumber)*100
	InhibitcellsP = Inhibitcells/cellNumber*100

	return ActivecellsP,InhibitcellsP

def TuningReliabilityActivated(array, threshold):
	#For each cell and each odorant, for how many trials was the response above threshold ?
	#output is cell x odors
	numcells = array.shape[1]
	numodors = array.shape[2]
	testodors =[]
	for l in range (numcells):
		for i in range (numodors):
			testodor= np.count_nonzero(array[:,l,i]>threshold )
			testodors.append(testodor)
	x = np.reshape(testodors, (numcells, numodors))

	# Howw many cells are above threshold for 0,1,... x trials ?
	#output is stimulus type x number of trials
	times= array.shape[0]+1
	counttrials=[]
	for l in range (numodors):
		for i in range (times):
			counttrial = np.count_nonzero(x[:,l]==i)
			counttrials.append(counttrial)
	xx = np.reshape(counttrials, (numodors, times))
	xxx=(xx*100)/numcells
	return xx, xxx

def TuningReliabilitySuppressed(array, threshold):
	#For each cell and each odorant, for how many trials was the response above threshold ?
	#output is cell x odors
	numcells = array.shape[1]
	numodors = array.shape[2]
	testodors =[]
	for l in range (numcells):
		for i in range (numodors):
			testodor= np.count_nonzero(array[:,l,i]<threshold )
			testodors.append(testodor)
	x = np.reshape(testodors, (numcells, numodors))

	# Howw many cells are above threshold for 0,1,... x trials ?
	#output is stimulus type x number of trials
	times= array.shape[0]+1
	counttrials=[]
	for l in range (numodors):
		for i in range (times):
			counttrial = np.count_nonzero(x[:,l]==i)
			counttrials.append(counttrial)
	xx = np.reshape(counttrials, (numodors, times))
	xxx=(xx*100)/numcells
	return xx, xxx

def TuningCurveActivated(array, threshold, trialthreshold):
	numcells = array.shape[1]
	numodors = array.shape[2]
	testodors =[]
	for l in range (numcells):
		for i in range (numodors):
			testodor= np.count_nonzero(array[:,l,i]>threshold)
			testodors.append(testodor)
	x = np.reshape(testodors, (numcells, numodors))

	countts=[]
	for l in range (numcells):
		count = np.count_nonzero(x[l,:]>=trialthreshold)
		countts.append(count)
	return countts

def TuningCurveSuppressed(array, threshold, trialthreshold):
	numcells = array.shape[1]
	numodors = array.shape[2]
	testodors =[]
	for l in range (numcells):
		for i in range (numodors):
			testodor= np.count_nonzero(array[:,l,i]<threshold)
			testodors.append(testodor)
	x = np.reshape(testodors, (numcells, numodors))

	countts=[]
	for l in range (numcells):
		count = np.count_nonzero(x[l,:]>=trialthreshold)
		countts.append(count)
	return countts

def PlotMeanwConfidence(array):
    cellnum = np.count_nonzero(array, axis=1)
    cellnum = cellnum[0]
    framenum = np.count_nonzero(array, axis=0)
    framenum = framenum[0]
    vect_len = framenum
    x = np.arange(0, vect_len, 1)
    dataframe1= pd.DataFrame(array, index=x)
    for index, col_name in enumerate(dataframe1):
        if index == 0:
            y_cat = dataframe1[col_name]
        else:
            y_cat = np.append(y_cat, dataframe1[col_name])
    x_cat = np.tile(x, cellnum)
    dataframe2 = pd.DataFrame({"x": x_cat, "Dff": y_cat})
    
    return dataframe2

def thresholdOutlierResponses(array, lthresh = 1, hthresh = 99):
	threshh = np.percentile(array, hthresh)
	threshl = np.percentile(array, lthresh)

	excludeE = np.unique(np.argwhere((array>threshh).sum(2)).squeeze()[:,1])
	excludeI = np.unique(np.argwhere((array<threshl).sum(2)).squeeze()[:,1])

	exclude = np.union1d(excludeE,excludeI)

	array = np.delete(array, exclude, axis = 1)
	return array

def getSNR(resps, numTrialsToKeep = 8):
	#resps is array shaped trials, cells, odors

	odormeans = resps.mean(0)
	resids = (odormeans[None]-resps)
	ratio  = np.mean(((odormeans**2)**.5  /  ((resids**2).mean(0)**.5)), 1)
	cellSNR = 10*np.log10(ratio)
	return cellSNR

#data is array shaped trials, cells, odors
def get_sigCors(data):
    data2 = data.mean(0)
    return squareform(np.corrcoef(data2), checks=False)

#data is array shaped trials, cells, odors
def get_EnsembleCors(data):
    data2 = data.mean(0).T
    return squareform(np.corrcoef(data2), checks=False)

def getLS(data, No):
	# neg resps zeroed
	input_ = data.copy().T  # input: odors by neurons by trials

	# zero negatives# or, take the modulo.
	input_ = input_.mean(-1)

	input_[input_ < 0] = 0
	input_ = np.delete(input_, np.argwhere(np.mean(input_ ** 2, 0) == 0), axis=1)
	numerator = 1 - input_.mean(0) ** 2 / np.mean(input_ ** 2, 0)
	denominator = 1 - 1 / No

	LS = numerator / denominator
	return LS

def getPS_abs(data, No):
	input_ = data.T.copy()  # input: odors by neurons by trials

	# zero negatives# or, take the modulo.
	input_ = np.abs(input_.mean(-1))
	input_ = np.delete(input_, np.argwhere(np.mean(input_ ** 2, 0) == 0), axis=1)
	# here, simply flip odor and neuron dimension
	input_ = input_.T
	numerator = 1 - input_.mean(0) ** 2 / np.mean(input_ ** 2, 0)
	denominator = 1 - 1 / No

	PS = numerator / denominator
	return PS

def getNoiseCorrelations(array):
    from scipy.spatial.distance import squareform
    respMeans = array.mean(0)
    residuals = array-respMeans
    ntrials, ncells, nodors = array.shape

    reshapedResiduals = residuals.transpose(1,2,0).reshape(ncells, -1)
    corrMatrix = np.corrcoef(reshapedResiduals)
    condensedMatrix = squareform(corrMatrix, checks = False)
    return condensedMatrix


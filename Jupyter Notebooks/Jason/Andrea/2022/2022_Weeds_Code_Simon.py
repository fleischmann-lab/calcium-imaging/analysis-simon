#Dataset is store in Google drive: https://drive.google.com/drive/folders/1YkI3wgg09NPPk-zc4bsTmVlrJ-a5aEZB?usp=sharing
#Change the following 3 parameters before running the code
# The code runs on multicore processing, therefore change the Number_Computer_Core accordingly 
#Data_Input_Folder is where the above downloaded folder should be
#Saving_Folder is where the figure will be saved

#Define parameters and folders
Number_Computer_Core = 16
Data_Input_Folder = '/Users/sdaste/Desktop/Jason/2021_Passive_Dataset/'
Saving_Folder = '/Users/sdaste/Desktop'

# Import module
import os
import numpy as np
import pandas as pd
from numpy import load
import seaborn as sns
from matplotlib import pylab as plt
from joblib import Parallel, delayed
from tqdm import tqdm
from sklearn.svm import SVC
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()

# Define functions
def importfile(FOLDER_PATH_plane):
    filteredDataArraypath = os.path.join(FOLDER_PATH_plane, "filteredDataArray.npy")
    DataArray_Redpath = os.path.join(FOLDER_PATH_plane, "DataArray_Red.npy")
    DataArray_Greenpath = os.path.join(FOLDER_PATH_plane, "DataArray_Green.npy")

    filteredDataArray = load(filteredDataArraypath)
    DataArray_Red = load(DataArray_Redpath)
    DataArray_Green = load(DataArray_Greenpath)

    return filteredDataArray, DataArray_Red, DataArray_Green

def getRespsRegular(array, numTrialsToKeep, blinewin, odorwin):
	#array: trials, cells, odors, frames

	# average F every this frames after odor onset
	# numTrialsToKeep: optional 

	ntrials, ncells, nodors, nframes = array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps = array[:numTrialsToKeep][...,odorwin[0]:odorwin[1]].mean(-1) -  array[:numTrialsToKeep][..., blinewin[0]:blinewin[1]].mean(-1)
	return resps

def MeanResp(array, numTrialsToKeep, win): 
	#array: trials, cells, odors, frames

	ntrials, ncells, nodors, nframes = array.shape
	if not numTrialsToKeep:
		numTrialsToKeep=ntrials

	# set preodor baseline to zero and account for some nan frames by offsett
	resps = array[:numTrialsToKeep][...,win[0]:win[1]].mean(-1)

	return resps

def ClassifierInputswRoll(Array, RespArray, NumberOdor, NumberTrials):

    xx = np.arange(0,NumberOdor)
    yy = np.ones((1,NumberTrials),dtype=np.int32)
    zz = np.kron(xx,yy)
    y = np.ravel(zz.transpose(1, 0))

    inputArrayForRespsClass = Array.transpose(1,0,2,3)
    stepsize = 3
    respsbaserolling = []
    for i in range (100):
        respsbase = MeanResp(inputArrayForRespsClass, numTrialsToKeep=NumberTrials, win = [i,i+stepsize])
        respsbaserolling.append(respsbase[None])
    respsbaserolling=np.concatenate(respsbaserolling,0)

    respsbaseclas= respsbaserolling.transpose (3,1,2,0)
    respsbaseclass = respsbaseclas.reshape(-1, respsbaseclas.shape[-2],respsbaseclas.shape[-1])

    respsclas= RespArray.transpose (2,0,1)
    respsclass = respsclas.reshape(-1, respsclas.shape[-1])

    X = respsclass
    Xz = respsbaseclass

    return X, Xz, y

def SVMrandselectcellsPARALLEL(X, y, n_splits, n_repeats, numcells):
        index = np.random.choice(X.shape[1], numcells+1, replace=False)
        X_rand = X[:, index]
        scores = np.zeros([n_splits * n_repeats])
        svm_class = SVC(kernel='linear')
        rskf = RepeatedStratifiedKFold(n_splits=n_splits, n_repeats=n_repeats)
        for idx, (train_index, test_index) in enumerate(rskf.split(X_rand, y)):
            X_train = X_rand[train_index]
            X_test = X_rand[test_index]
            y_train, y_test = y[train_index], y[test_index]
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
            svm_class.fit(X_train, y_train)
            y_pred = svm_class.predict(X_test)
            scores[idx] = accuracy_score(y_test, y_pred)
        swinscore = np.mean(scores)
        return swinscore

def PlotMeanwConfidence(array):
    cellnum = np.count_nonzero(array, axis=1)
    cellnum = cellnum[0]
    framenum = np.count_nonzero(array, axis=0)
    framenum = framenum[0]
    vect_len = framenum
    x = np.arange(0, vect_len, 1)
    dataframe1= pd.DataFrame(array, index=x)
    for index, col_name in enumerate(dataframe1):
        if index == 0:
            y_cat = dataframe1[col_name]
        else:
            y_cat = np.append(y_cat, dataframe1[col_name])
    x_cat = np.tile(x, cellnum)
    dataframe2 = pd.DataFrame({"x": x_cat, "Dff": y_cat})
    
    return dataframe2

# Import data - Load OB mice
filteredDataArray0, DataArray_Red0, DataArray_Green0 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#8/Plane_0"))
filteredDataArray1, DataArray_Red1, DataArray_Green1 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#8/Plane_2"))
filteredDataArray2, DataArray_Red2, DataArray_Green2 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#9/Plane_0"))
filteredDataArray3, DataArray_Red3, DataArray_Green3 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#9/Plane_1"))
filteredDataArray4, DataArray_Red4, DataArray_Green4 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#9/Plane_2"))
filteredDataArray5, DataArray_Red5, DataArray_Green5 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#163/Plane_0"))
filteredDataArray6, DataArray_Red6, DataArray_Green6 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#163/Plane_1"))
filteredDataArray7, DataArray_Red7, DataArray_Green7 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#163/Plane_2"))
filteredDataArray8, DataArray_Red8, DataArray_Green8 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#164/Plane_0"))
filteredDataArray9, DataArray_Red9, DataArray_Green9 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#164/Plane_2"))
filteredDataArray10, DataArray_Red10, DataArray_Green10 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#513/Plane_0"))
filteredDataArray11, DataArray_Red11, DataArray_Green11 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#513/Plane_1"))
filteredDataArray12, DataArray_Red12, DataArray_Green12 = importfile(os.path.join(Data_Input_Folder, "OB_dataset/Mouse#513/Plane_2"))

filteredDataArray_OB = np.concatenate((filteredDataArray0, filteredDataArray1, filteredDataArray2, filteredDataArray3, filteredDataArray4, filteredDataArray5, filteredDataArray6, filteredDataArray7, filteredDataArray8, filteredDataArray9), axis = 0)

DataArray_Red_OB = np.concatenate((DataArray_Red0,DataArray_Red1, DataArray_Red2, DataArray_Red3, DataArray_Red4, DataArray_Red5, DataArray_Red6, DataArray_Red7, DataArray_Red8, DataArray_Red9), axis = 0)
DataArray_Mouse513red = np.concatenate((DataArray_Red10,DataArray_Red11,DataArray_Red12), axis=0)
DataArray_Mouse5133red= DataArray_Mouse513red[:,0:8,:,:]
DataArray_Red_OB = DataArray_Red_OB[:,:,:,22:122]
DataArray_Red_OB = np.concatenate((DataArray_Mouse5133red,DataArray_Red_OB), axis=0)


DataArray_Mouse513 = np.concatenate((DataArray_Green10,DataArray_Green11,DataArray_Green12), axis=0)
DataArray_Mouse5133= DataArray_Mouse513[:,0:8,:,:]
DataArray_Green_OB = np.concatenate((DataArray_Green0, DataArray_Green1, DataArray_Green2, DataArray_Green3, DataArray_Green4, DataArray_Green5, DataArray_Green6, DataArray_Green7, DataArray_Green8, DataArray_Green9), axis =0)
DataArray_Green_OB = DataArray_Green_OB[:,:,:,22:122]
DataArray_Green_OB = np.concatenate((DataArray_Mouse5133,DataArray_Green_OB), axis=0)

# Import date - Load mPFC mice
filteredDataArray0_mPFC, DataArray_Red0_mPFC, DataArray_Green0_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#443/Plane_0"))
filteredDataArray1_mPFC, DataArray_Red1_mPFC, DataArray_Green1_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#443/Plane_1"))
filteredDataArray2_mPFC, DataArray_Red2_mPFC, DataArray_Green2_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#448/Plane_0"))
filteredDataArray3_mPFC, DataArray_Red3_mPFC, DataArray_Green3_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#448/Plane_1"))
filteredDataArray4_mPFC, DataArray_Red4_mPFC, DataArray_Green4_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#512/Plane_0"))
filteredDataArray5_mPFC, DataArray_Red5_mPFC, DataArray_Green5_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#512/Plane_1"))
filteredDataArray6_mPFC, DataArray_Red6_mPFC, DataArray_Green6_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#533/Plane_0"))
filteredDataArray7_mPFC, DataArray_Red7_mPFC, DataArray_Green7_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#533/Plane_1"))
filteredDataArray8_mPFC, DataArray_Red8_mPFC, DataArray_Green8_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#644/Plane_0"))
filteredDataArray9_mPFC, DataArray_Red9_mPFC, DataArray_Green9_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#644/Plane_1"))
filteredDataArray10_mPFC, DataArray_Red10_mPFC, DataArray_Green10_mPFC = importfile(os.path.join(Data_Input_Folder, "mPFC_dataset/Mouse#644/Plane_2"))

filteredDataArray_mPFC = np.concatenate((filteredDataArray0_mPFC, filteredDataArray1_mPFC, filteredDataArray2_mPFC, filteredDataArray3_mPFC, filteredDataArray4_mPFC, filteredDataArray5_mPFC, filteredDataArray6_mPFC, filteredDataArray7_mPFC, filteredDataArray8_mPFC, filteredDataArray9_mPFC, filteredDataArray10_mPFC), axis = 0)

DataArray_Red_mPFC = np.concatenate((DataArray_Red0_mPFC,DataArray_Red1_mPFC, DataArray_Red2_mPFC, DataArray_Red3_mPFC, DataArray_Red4_mPFC, DataArray_Red5_mPFC, DataArray_Red6_mPFC, DataArray_Red7_mPFC, DataArray_Red8_mPFC, DataArray_Red9_mPFC, DataArray_Red10_mPFC), axis = 0)

DataArray_Green_mPFC = np.concatenate((DataArray_Green0_mPFC, DataArray_Green1_mPFC, DataArray_Green2_mPFC, DataArray_Green3_mPFC, DataArray_Green4_mPFC, DataArray_Green5_mPFC, DataArray_Green6_mPFC, DataArray_Green7_mPFC, DataArray_Green8_mPFC, DataArray_Green9_mPFC, DataArray_Green10_mPFC), axis =0)

# Remove last two trials from mPFC mice 
DataArray_Green_mPFC = DataArray_Green_mPFC[:,0:8,:,:]
DataArray_Red_mPFC = DataArray_Red_mPFC[:,0:8,:,:]

# Remove controls
filteredDataArray_OB = filteredDataArray_OB[:,:,2:10,:]
DataArray_Red_OB = DataArray_Red_OB[:,:,2:10,:]
DataArray_Green_OB = DataArray_Green_OB[:,:,2:10,:]

filteredDataArray_mPFC = filteredDataArray_mPFC[:,:,1:9,:]
DataArray_Red_mPFC = DataArray_Red_mPFC[:,:,1:9,:]
DataArray_Green_mPFC = DataArray_Green_mPFC[:,:,1:9,:]

#Concatenate all neurons of all mice
DataArray_Green_all = np.concatenate((DataArray_Green_OB, DataArray_Green_mPFC), axis=0)
DataArray_Red_all = np.concatenate((DataArray_Red_OB, DataArray_Red_mPFC), axis=0)
DataArray_All = np.concatenate((DataArray_Red_all,DataArray_Green_all),axis=0)

# Calculate neuronal response matrices
numTrialsToKeep = 8
blinewin = [0,15]
odorwin = [30,45]

inputArrayForRespsAll = DataArray_All.transpose(1,0,2,3)
respsAll = getRespsRegular(inputArrayForRespsAll, numTrialsToKeep=numTrialsToKeep, odorwin=odorwin, blinewin=blinewin)

inputArrayForRespsOB = DataArray_Red_OB.transpose(1,0,2,3)
respsOB = getRespsRegular(inputArrayForRespsOB, numTrialsToKeep=numTrialsToKeep, odorwin=odorwin, blinewin=blinewin)

inputArrayForRespsmPFC = DataArray_Red_mPFC.transpose(1,0,2,3)
respsmPFC = getRespsRegular(inputArrayForRespsmPFC, numTrialsToKeep=numTrialsToKeep, odorwin=odorwin, blinewin=blinewin)


# Create classifier inputs for each neuronal population - X(samples,features), y class label
X_All, Xz_All, y_All = ClassifierInputswRoll(Array=DataArray_All, RespArray=respsAll, NumberOdor=8, NumberTrials=8)
X_OB, Xz_OB, y_OB = ClassifierInputswRoll(Array=DataArray_Red_OB, RespArray=respsOB, NumberOdor=8, NumberTrials=8)
X_mPFC, Xz_mPFC, y_mPFC = ClassifierInputswRoll(Array=DataArray_Red_mPFC, RespArray=respsmPFC, NumberOdor=8, NumberTrials=8)


#Run classifier - multicore processing
numRand = 2
numcells=68
n_splits = 8
n_repeats = 5

swintestP_All = np.zeros([numRand, numcells])
swintestP_OB = np.zeros([numRand, numcells])
swintestP_mPFC = np.zeros([numRand, numcells])

for l in tqdm(range(numRand)):  # l is the number of time to randomize the cell population
    swintestP_All[l, :] = Parallel(n_jobs=Number_Computer_Core)(delayed(SVMrandselectcellsPARALLEL)(X=X_All, y=y_All, n_splits=n_splits, n_repeats=n_repeats,numcells=numcells) for numcells in range(numcells))


for l in tqdm(range(numRand)):  # l is the number of time to randomize the cell population
    swintestP_OB[l, :] = Parallel(n_jobs=Number_Computer_Core)(delayed(SVMrandselectcellsPARALLEL)(X=X_OB, y=y_OB, n_splits=n_splits, n_repeats=n_repeats,numcells=numcells) for numcells in range(numcells))


for l in tqdm(range(numRand)):  # l is the number of time to randomize the cell population
    swintestP_mPFC[l, :] = Parallel(n_jobs=Number_Computer_Core)(delayed(SVMrandselectcellsPARALLEL)(X=X_mPFC, y=y_mPFC, n_splits=n_splits, n_repeats=n_repeats,numcells=numcells) for numcells in range(numcells))

#Rearange array for plotting
swintest_All = swintestP_All.transpose(1,0)
dataframe_All = PlotMeanwConfidence(array=swintest_All)

swintest_OB = swintestP_OB.transpose(1,0)
dataframe_OB = PlotMeanwConfidence(array=swintest_OB)

swintest_mPFC = swintestP_mPFC.transpose(1,0)
dataframe_mPFC= PlotMeanwConfidence(array=swintest_mPFC)

#Plot figure
font1 = {'family': 'arial',
        'color':  'black',
        'weight': 'normal',
        'size': 16,
        }
font2 = {'family': 'arial',
        'color':  'mediumseagreen',
        'weight': 'normal',
        'size': 22,
        }
font3 = {'family': 'arial',
        'color':  'firebrick',
        'weight': 'normal',
        'size': 22,
        }
font4 = {'family': 'arial',
        'color':  'royalblue',
        'weight': 'normal',
        'size': 22,
        }

plt.figure(figsize=(8,8))
sns.lineplot(x="x", y="Dff", data=dataframe_All, color = 'mediumseagreen')
sns.lineplot(x="x", y="Dff", data=dataframe_OB, color = 'firebrick')
sns.lineplot(x="x", y="Dff", data=dataframe_mPFC, color = 'royalblue')
plt.xlabel('Population size', fontdict=font1)
plt.ylabel('Classification accuracy (%)',fontdict=font1)
plt.xticks(fontsize = 15)
plt.yticks(fontsize = 15)
plt.hlines(0.125,0,68, linestyles='dashed')
plt.text(0, 0.48, r'PCx', fontdict=font2)
plt.text(0, 0.46, r'PCxmPFC', fontdict=font4)
plt.text(0, 0.44, r'PCxOB', fontdict=font3)
plt.text(55, 0.130, r'chance', fontdict=font1)

#Save figure
savepath = os.path.join(Saving_Folder, "Comparison_pseudopopulation.pdf")
plt.savefig(savepath, transparent = True, dpi = 300, bbox_inches='tight')
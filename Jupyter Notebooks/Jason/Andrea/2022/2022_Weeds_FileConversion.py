#This code takes - the XML microscope file - the raw Teeensy file - the Suite2p  Numpy files (for one plane at a time)
# and create first a master dataframe containing all aligned information. 
#It then creates a Numpy 4Darray and defines tdTomato positive cells based on fluorescence intensity and non-correlation with GCaMP signal.
# Finally, 4Darray for GCaMP+/tdTomato-, GCaMP+/tdTomato+ neurons are created and saved


import sys
import os
import numpy as np
import csv
from xml.dom import minidom
import pandas as pd
from sklearn.preprocessing import scale
from sklearn import preprocessing
from scipy import stats
from numpy import save
import seaborn as sns
sns.set_style('ticks')
sns.set_context('talk')

#Path of input files
FOLDER_PATH = "/Users/sdaste/Desktop/Test/suite2p/plane0" #Suite2P

odorPath = '/Users/sdaste/Desktop/Test/20210511_Mouse#513_Teensy_serial_data.csv' #Teensy file

xmlPath = '/Users/sdaste/Desktop/Test/TSeries-05112021-1455-001.xml' #Scope XML

saveDataFramePath = '/Users/sdaste/Desktop/Test' #DataFrame saving

# define some arguments for later:
numFramesOdorTrial = 100
numOdors = 10
numTrials = 10
numOdorTrials = 100

#Read Suite2P files
path1 = os.path.join(FOLDER_PATH, "F.npy")
path2 = os.path.join(FOLDER_PATH, "Fneu.npy")
path3 = os.path.join(FOLDER_PATH, "F_chan2.npy")
path4 = os.path.join(FOLDER_PATH, "Fneu_chan2.npy")
path5 = os.path.join(FOLDER_PATH, "spks.npy")
path6 = os.path.join(FOLDER_PATH, "iscell.npy")
path7 = os.path.join(FOLDER_PATH, "stat.npy")

traces = np.load(path1)
neu = np.load(path2)
trace_red = np.load(path3)
neu_red = np.load(path4)
spks = np.load(path5)
ithisaCellBinary = np.load(path6)
stat_dict = np.load(path7, allow_pickle = True)[0]

numCells, numFramesTotal = traces.shape

#read Teensy file
#allocate an array to store the sequence of odors (10) for each trial (8)
trialOdorArray = np.zeros((numTrials,numOdors)).astype(int)

trialID = 0 # flag for indexing trialOdorArray
with open(odorPath) as f:
    readCSV = csv.reader(f, delimiter=",")
    for row in readCSV:
        if 'About to pick' not in row:
            continue
        else:
            thisTrialSequence = []
            for i in range(numOdors):
                thisOdor = int(next(readCSV)[0])
                trialOdorArray[trialID,i]=thisOdor
            trialID+=1 # update flag

#read XML file
doc = minidom.parse(xmlPath)
sequence = doc.getElementsByTagName('Sequence')
numFrames_XML = len(sequence)
# is number of suite2p frames equal to the number of xml frames?
print(f'correct number of frames: {numFramesTotal==numFrames_XML}')


# get xmlFrameIDS, framePeriods, relativeTimes, absoluteTimes. 
#These values are specific to each experiment. 
#We assume there is a 1 to 1 correspondence between xml frames and suite2p frames
def getMetaDataFromXML(sequence,numFrames_XML):
    """
    Returns: xml Frame ID,
    duration of each Frame in seconds,
    relative time, 
    absolute time
    """
    framePeriods = []
    relativeTimes = []
    absoluteTimes = []
    xmlFrameIDS = []
    for xmlFrame in range(numFrames_XML):
        subFrames = sequence[xmlFrame].getElementsByTagName('Frame')
        xmlFrameID = int(sequence[xmlFrame].getAttribute('cycle'))

        #for each xml sequence there are 3 subfields called Frame. Each one  has a framePeriod associated with it. Add these to get the total time for each sequence.
        framePeriod = 0
        for subframe in subFrames:
            a = subframe.getElementsByTagName('PVStateValue')[0]
            framePeriod+=float(a.attributes.items()[1][1])

            # for the last subframe, get the relative and absolute times. 
        lastFrame = subFrames[2]
        relativeTime = float(lastFrame.getAttribute('relativeTime'))
        absoluteTime = float(lastFrame.getAttribute('absoluteTime'))

        framePeriods.append(framePeriod)
        relativeTimes.append(relativeTime)
        absoluteTimes.append(absoluteTime)
        xmlFrameIDS.append(xmlFrameID)
    return xmlFrameIDS, framePeriods, relativeTimes, absoluteTimes

xmlFrameIDS, framePeriods, relativeTimes, absoluteTimes = getMetaDataFromXML(sequence,numFrames_XML)

#Create Master DataFrame
dfsAllCells = []
for cell in range(numCells):
    dfOneCell = pd.DataFrame(dict(xmlFrameID = xmlFrameIDS,
                                    suite2pFrameID = None, 
                                    framePeriod = framePeriods,
                                    frameRate = 1/np.array(framePeriods),
                                    relativeTime = relativeTimes,
                                    absoluteTime = absoluteTimes,
                                    cellID = cell,
                                    F = traces[cell,:],
                                    Fneu = neu[cell,:],
                                    F_red = trace_red[cell,:],
                                    Fneu_red = neu_red[cell,:],
                                    deconv = spks[cell,:],
                                    odorPos = None, # the position of each odor in the pseudorandomized sequence associated with each trial.
                                    odorID = None, # the actual odorID in this position.
                                    trialID = None,
                                    cellProbBinary = ithisaCellBinary[cell][0],
                                    cellProbRaw = ithisaCellBinary[cell][1],

                                ))

    odorTrialID = 0
    for trialID in range(numTrials):
        for odorPos in range(numOdors):
            trialOdorFrameRange = np.s_[numFramesOdorTrial*odorTrialID:numFramesOdorTrial*odorTrialID +numFramesOdorTrial]
            # for column indexing below, I need to provide the index corresponding to the name of each column, hence: list(dfOnceCell.columns).index(columnName)
            dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('suite2pFrameID')] = range(numFramesOdorTrial) #assign frame id to each odor-trial
            dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('trialID')] = trialID
            dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('odorPos')] = odorPos
            dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('odorID')] = trialOdorArray[trialID,odorPos] # index to specific trial-odor id. 
            odorTrialID+=1
    dfsAllCells.append(dfOneCell)
    
# combine all cells into one Dataframe
DF = pd.concat(dfsAllCells,axis=0)
#subtract neuropil
DF['Fadj'] = DF.F- 0.7*DF.Fneu
DF['Fadj_red'] = DF.F_red- 0.7*DF.Fneu_red

#Sort DataFrame on cellID then trialID, then odoID, 
DFsorted = DF.sort_values(['cellID', 'trialID', 'odorID', 'suite2pFrameID'])


#Create 4D Numpy Array
numCells = DF["cellID"].iloc[-1]+1

DataArray_Fadj = []
DataArray_Fadj_Fluo = []
for cell in range(numCells):
    Fadj_trace = DFsorted.loc[DFsorted.cellID==cell,'Fadj'].values
    
    
    # scale each cell's signal in its entirety
    Fadj_trace_scale=scale(Fadj_trace)

    DataArray_Fadj.append(Fadj_trace_scale.reshape(numTrials,numOdors,numFramesOdorTrial)[None])
    DataArray_Fadj_Fluo.append(Fadj_trace.reshape(numTrials,numOdors,numFramesOdorTrial)[None])
DataArray_Fadj = np.concatenate(DataArray_Fadj,0)
DataArray_Fadj_Fluo = np.concatenate(DataArray_Fadj_Fluo,0)

#DO THE SAME FOR RED SIGNAL - ONLY FLUO NO SCALE
DataArray_Fadj_Fluo_red = []
for cell in range(numCells):
    Fadj_trace_red = DFsorted.loc[DFsorted.cellID==cell,'Fadj_red'].values
    DataArray_Fadj_Fluo_red.append(Fadj_trace_red.reshape(numTrials,numOdors,numFramesOdorTrial)[None])

DataArray_Fadj_Fluo_red = np.concatenate(DataArray_Fadj_Fluo_red,0)

#Filter array for true Suite2P neurons (in iscell = 1)
cellsToKeep = DFsorted.loc[DFsorted.cellProbBinary>0.5].cellID.unique()

filteredDataArray = DataArray_Fadj[cellsToKeep,:,:,:]
filteredDataArray_Fluo = DataArray_Fadj_Fluo[cellsToKeep,:,:,:]
filteredDataArray_Fluo_red = DataArray_Fadj_Fluo_red[cellsToKeep,:,:,:]

cellsToKeepTrue = np.count_nonzero(cellsToKeep, axis=0)
cellsToKeepTrue = cellsToKeepTrue+1


#Calculate Pearson coefficient correlation between tdTomato and GCaMP signal
result_array_correlation = np.zeros((cellsToKeepTrue,numOdors,numTrials))
for cell in range(cellsToKeepTrue):
    for odor in range(numOdors):
        for trial in range(numTrials):
            result_array_correlation[cell,odor,trial] = stats.pearsonr(filteredDataArray_Fluo[cell,trial,odor,:],filteredDataArray_Fluo_red[cell,trial,odor,:])[0] #0 = r, 1= p-value

result_array_correlation1 = result_array_correlation.mean(2)
result_array_correlation2 = result_array_correlation1.mean(1)

#Calculate fluorescence distribution of TdTomato
FluoDistr= filteredDataArray_Fluo_red.mean(2)
FluoDistr= FluoDistr[...,0:23]
FluoDistr= FluoDistr.mean(2)
FluoDistr= FluoDistr.mean(1)
FluoDistr.shape

# FluoDistr = FluoDistr[FluoDistr >= 0]
FluoDistrNorm = preprocessing.minmax_scale(FluoDistr, feature_range=(0, 1),axis=0, copy=True)


#Get TdTomato positive neuron list
# Necessary to do it on all cells (aka with false positives)
Redcellsmean= DataArray_Fadj_Fluo_red.mean(2)
Redcellsmean= Redcellsmean[...,0:23]
Redcellsmean= Redcellsmean.mean(2)
Redcellsmean= Redcellsmean.mean(1)

RedCellId = []
RedCellProb = []
for cell in range (numCells):
    RedCellId1= DF.loc [DF.cellID==cell, 'cellID'].values
    RedCellProb1= DF.loc [DF.cellID==cell, 'cellProbBinary'].values
    RedCellId.append(RedCellId1.reshape(numTrials,numOdors,numFramesOdorTrial)[None])
    RedCellProb.append(RedCellProb1.reshape(numTrials,numOdors,numFramesOdorTrial)[None])

RedCellId = np.concatenate(RedCellId,0)
RedCellProb = np.concatenate(RedCellProb,0)

RedCellId = RedCellId[:,0,0,0]
RedCellProb = RedCellProb[:,0,0,0]

print(f'4D matrices are the same size: {RedCellId.shape==Redcellsmean.shape}')

result_array_correlationforRED = np.zeros((numCells,numOdors,numTrials))
for cell in range(numCells):
    for odor in range(numOdors):
        for trial in range(numTrials):
            result_array_correlationforRED[cell,odor,trial] = stats.pearsonr(DataArray_Fadj_Fluo[cell,odor,trial,:],DataArray_Fadj_Fluo_red[cell,odor,trial,:])[0] #0 = r, 1= p-value
result_array_correlationforRED.shape

result_array_correlationforRED1 = result_array_correlationforRED.mean(2)
result_array_correlationforRED2 = result_array_correlationforRED1.mean(1)

RedCellList = pd.DataFrame(np.vstack((RedCellId[:], RedCellProb[:], Redcellsmean[:], result_array_correlationforRED2[:]))).T


# Put Fluorescence intensity threshold to select true positive tdTomato cells
RedThrslhd = 300
CorrThrslhd = 0.5

RedCellListFinal = RedCellList[RedCellList[1] == 1.0]
RedCellListFinal = RedCellListFinal[RedCellListFinal[2] >= RedThrslhd]
RedCellListFinal = RedCellListFinal[RedCellListFinal[3] <= CorrThrslhd]
RedCellListFinal = RedCellListFinal.rename({0: 'cellID', 1: 'GreencellProb', 2: 'RedcellMeanFluo', 3:'CorrelationGreenRed'}, axis=1)


# Create list of Redcells to keep from above
RedcellsToKeep = RedCellListFinal['cellID']
RedcellsToKeep = RedcellsToKeep.to_numpy()
RedcellsToKeep = RedcellsToKeep.astype(int)

DataArray_Red = DataArray_Fadj[RedcellsToKeep,:,:,:]

#Create list of Greencells excluding Redcells

GreenCellListFinal = RedCellList[RedCellList[1] == 1.0]
GreenCellListFinal = GreenCellListFinal[GreenCellListFinal[2] < RedThrslhd]
GreenCellListFinal = GreenCellListFinal.rename({0: 'cellID', 1: 'GreencellProb', 2: 'RedcellMeanFluo'}, axis=1)

GreencellsToKeep = GreenCellListFinal['cellID']
GreencellsToKeep = GreencellsToKeep.to_numpy()
GreencellsToKeep = GreencellsToKeep.astype(int)

DataArray_Green = DataArray_Fadj[GreencellsToKeep,:,:,:]

#Final matrices for further analysis

DataArray_Green = DataArray_Green.transpose(0,2,1,3)
DataArray_Red = DataArray_Red.transpose(0,2,1,3)
filteredDataArray = filteredDataArray.transpose(0,2,1,3)

#Save

filteredDataArraypath = os.path.join(saveDataFramePath, "filteredDataArray.npy")
DataArray_Redpath = os.path.join(saveDataFramePath, "DataArray_Red.npy")
DataArray_Greenpath = os.path.join(saveDataFramePath, "DataArray_Green.npy")
RedCellListSavepath = os.path.join(saveDataFramePath, "RedCellList.npy")

#All Cells 
                            #filteredDataArray
save(file = filteredDataArraypath, arr= filteredDataArray)

#Only Red cells
                            #DataArray_Red
save(file = DataArray_Redpath, arr= DataArray_Red)

#Only Green non-Red cells
                            #DataArray_Green
save(file = DataArray_Greenpath, arr= DataArray_Green)

#List of tdTomato cells
                            #RedCellList
save(file = RedCellListSavepath, arr= RedCellListFinal)
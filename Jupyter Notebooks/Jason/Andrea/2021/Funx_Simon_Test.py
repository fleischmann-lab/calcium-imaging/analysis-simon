


def getMetaDataFromXML(sequence,numFrames_XML):
    """
    Returns: xml Frame ID,
    duration of each Frame in seconds,
    relative time, 
    absolute time
    """
    framePeriods = []
    relativeTimes = []
    absoluteTimes = []
    xmlFrameIDS = []
    for xmlFrame in range(numFrames_XML):
        subFrames = sequence[xmlFrame].getElementsByTagName('Frame')
        xmlFrameID = int(sequence[xmlFrame].getAttribute('cycle'))

        #for each xml sequence there are 3 subfields called Frame. Each one  has a framePeriod associated with it. Add these to get the total time for each sequence.
        framePeriod = 0
        for subframe in subFrames:
            a = subframe.getElementsByTagName('PVStateValue')[0]
            framePeriod+=float(a.attributes.items()[1][1])

            # for the last subframe, get the relative and absolute times. 
        lastFrame = subFrames[2]
        relativeTime = float(lastFrame.getAttribute('relativeTime'))
        absoluteTime = float(lastFrame.getAttribute('absoluteTime'))

        framePeriods.append(framePeriod)
        relativeTimes.append(relativeTime)
        absoluteTimes.append(absoluteTime)
        xmlFrameIDS.append(xmlFrameID)
    return xmlFrameIDS, framePeriods, relativeTimes, absoluteTimes

def NeuralPreprocessingSimon (FOLDER_PATH, odorPath, xmlPath, saveDataFramePath, numFramesOdorTrial, numOdors, numTrials, numOdorTrials):
	import sys
	import os

	import numpy as np
	import pandas as pd
	from glob import glob
	import seaborn as sns
	from matplotlib import pylab as plt
	from scipy.spatial.distance import squareform
	from numba import njit, prange

	sns.set_style('ticks')
	sns.set_context('talk')

	#Read Suite2P
	path1 = os.path.join(FOLDER_PATH, "F.npy")
	path2 = os.path.join(FOLDER_PATH, "Fneu.npy")
	path3 = os.path.join(FOLDER_PATH, "F_chan2.npy")
	path4 = os.path.join(FOLDER_PATH, "Fneu_chan2.npy")
	path5 = os.path.join(FOLDER_PATH, "spks.npy")
	path6 = os.path.join(FOLDER_PATH, "iscell.npy")
	path7 = os.path.join(FOLDER_PATH, "stat.npy")
	traces = np.load(path1)
	neu = np.load(path2)
	trace_red = np.load(path3)
	neu_red = np.load(path4)
	spks = np.load(path5)
	ithisaCellBinary = np.load(path6)
	stat_dict = np.load(path7, allow_pickle = True)[0]

	numCells, numFramesTotal = traces.shape

	# Read in odor sequence information.
	import csv
	trialOdorArray = np.zeros((numTrials,numOdors)).astype(int)

	trialID = 0 # flag for indexing trialOdorArray
	with open(odorPath) as f:
		readCSV = csv.reader(f, delimiter=",")
		for row in readCSV:
			if 'About to pick' not in row:
				continue
			else:
				thisTrialSequence = []
				for i in range(numOdors):
					thisOdor = int(next(readCSV)[0])
					trialOdorArray[trialID,i]=thisOdor
				trialID+=1 # update flag
	
	# Read in Prairie output for metadata. 
	from xml.dom import minidom
	doc = minidom.parse(xmlPath)
	sequence = doc.getElementsByTagName('Sequence')
	numFrames_XML = len(sequence)

	xmlFrameIDS, framePeriods, relativeTimes, absoluteTimes = getMetaDataFromXML(sequence,numFrames_XML)

	dfsAllCells = []
	for cell in range(numCells):
		dfOneCell = pd.DataFrame(dict(xmlFrameID = xmlFrameIDS,
										suite2pFrameID = None, 
										framePeriod = framePeriods,
										frameRate = 1/np.array(framePeriods),
										relativeTime = relativeTimes,
										absoluteTime = absoluteTimes,
										cellID = cell,
										F = traces[cell,:],
										Fneu = neu[cell,:],
										F_red = trace_red[cell,:],
										Fneu_red = neu_red[cell,:],
										deconv = spks[cell,:],
										odorPos = None, # the position of each odor in the pseudorandomized sequence associated with each trial.
										odorID = None, # the actual odorID in this position.
										trialID = None,
										cellProbBinary = ithisaCellBinary[cell][0],
										cellProbRaw = ithisaCellBinary[cell][1],

									))

		odorTrialID = 0
		for trialID in range(numTrials):
			for odorPos in range(numOdors):
				trialOdorFrameRange = np.s_[numFramesOdorTrial*odorTrialID:numFramesOdorTrial*odorTrialID +numFramesOdorTrial]
				# for column indexing below, I need to provide the index corresponding to the name of each column, hence: list(dfOnceCell.columns).index(columnName)
				dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('suite2pFrameID')] = range(numFramesOdorTrial) #assign frame id to each odor-trial
				dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('trialID')] = trialID
				dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('odorPos')] = odorPos
				dfOneCell.iloc[trialOdorFrameRange, list(dfOneCell.columns).index('odorID')] = trialOdorArray[trialID,odorPos] # index to specific trial-odor id. 
				odorTrialID+=1
		dfsAllCells.append(dfOneCell)
		
	# combine all cells into one Dataframe
	DF = pd.concat(dfsAllCells,axis=0)
	#subtract neuropil
	DF['Fadj'] = DF.F- 0.7*DF.Fneu
	DF['Fadj_red'] = DF.F_red- 0.7*DF.Fneu_red

	DF.to_hdf(saveDataFramePath, key='DF', mode='w')

def Create4DArray(DataFramePath,saveDataFramePathraw,numFramesOdorTrial, numOdors, numTrials, numOdorTrials):

	import sys
	import os

	import numpy as np
	import pandas as pd
	from glob import glob
	import seaborn as sns
	from matplotlib import pylab as plt
	from scipy.spatial.distance import squareform
	from scipy import stats
	from numba import njit
	from numpy import save
	from numpy import load
	from sklearn import preprocessing
	from sklearn.preprocessing import minmax_scale
	sns.set_style('ticks')
	sns.set_context('talk')

	DF= pd.read_hdf(DataFramePath, key='DF', mode='r')

	DFsorted = DF.sort_values(['cellID', 'trialID', 'odorID', 'suite2pFrameID'])


	from sklearn.preprocessing import scale
	numCells = DF["cellID"].iloc[-1]+1

	DataArray_Fadj = []
	for cell in range(numCells):
		Fadj_trace = DFsorted.loc[DFsorted.cellID==cell,'Fadj'].values
		
		
		# scale each cell's signal in its entirety
		Fadj_trace_scale=scale(Fadj_trace)

		DataArray_Fadj.append(Fadj_trace_scale.reshape(numTrials,numOdors,numFramesOdorTrial)[None])
	DataArray_Fadj = np.concatenate(DataArray_Fadj,0)

	filteredDataArraypath = os.path.join(saveDataFramePathraw, "filteredDataArray.npy")
	save(file = filteredDataArraypath, arr= DataArray_Fadj)
    
def correlogram (ArrayPath):
    import sys
    import os
    import numpy as np
    from numpy import load
    import seaborn as sns
    from matplotlib import pylab as plt
    sns.set_style('ticks')
    sns.set_context('talk')
    
    filteredDataArraypath = os.path.join(ArrayPath, "filteredDataArray.npy")
    filteredDataArray = load(filteredDataArraypath)
    
    odor = filteredDataArray[...,45:60].mean(-1)
    bline = filteredDataArray[...,25:40].mean(-1)
    respscorr= odor-bline
    
    plt.figure(figsize=(15,15))
    sns.heatmap(np.corrcoef(respscorr.transpose(0,2,1).reshape(respscorr.shape[0],-1).T[:]), square = True, xticklabels=[], yticklabels=[])
    for i in range(10):
        plt.axvline(8*i+8, color = 'w', linewidth = 1)
        plt.axhline(8*i+8, color = 'w', linewidth = 1)
